#ifndef __line_delimiter__
#define __line_delimiter__

struct LineDelimitationResult {
  char* result;
  bool ok;
};

class LineDelimiter {
  private:
    static const int capacity = 128;
    char buffer[capacity];
    int next;
    bool overflow;
    void reset();
  public:
    LineDelimiter();
    void addChar(char, LineDelimitationResult&);
};

#endif

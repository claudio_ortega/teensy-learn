#ifndef __cmd_parser__
#define __cmd_parser__

struct LineParsingResult {
  bool ok;
  char** result;
  int length;
  LineParsingResult();
  void reset();
};

class LineParser {
  private:
    static const int capacity = 128;
    char buffer[capacity];
    int next;
    bool overflow;
    void reset();
    void addChar(char);
  public:
    LineParser();
    void process(char*, char separator, LineParsingResult&);
};

#endif

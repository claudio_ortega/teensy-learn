#ifndef __util__
#define __util__

namespace util {
    struct AtoiResult{
        long value;
        bool valid;
    };

    void atoi( const char* a, AtoiResult& );
    void trim( char *str );
    void deleteRepeatedChars(char *str, char r);
    void deleteChars(char *str, char r);
}

#endif

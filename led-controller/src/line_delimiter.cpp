#include "line_delimiter.hpp"
#include "string.h"
#include "strings.h"

LineDelimiter::LineDelimiter() {
    reset();
}

void LineDelimiter::reset() {
    memset( buffer, 0, capacity );
    next = 0;
    overflow = false;
}

void LineDelimiter::addChar(char in, LineDelimitationResult& out) {

    out.ok = false;

    if ( in == '\x0d' ) {  // CR
        return;
    }

    if ( next > capacity-2 ) {
        overflow = true;
    }
    else {
       
        if ( in == '\x0a' ) {  // LF

            if ( overflow ) {
                reset();
                return;
            }

            out.ok = true;
            out.result = new char[strlen(buffer)+1];
            strcpy( out.result, buffer );

            reset();
            return;
        }

        buffer[next] = in;
        next++;
    }
}


#include <string.h>
#include <strings.h>
#include "line_parser.hpp"
#include "util.hpp"

LineParsingResult::LineParsingResult() {
    result = NULL;
    length = 0;
    ok = false;  
}

void LineParsingResult::reset() {
    if ( length > 0 ) {
        for ( int i=0; i<length; i++ ) {
            if ( result[i] != NULL ){
                delete result[i];
                result[i] = NULL;
            }
        }
        delete result;
        result = NULL;
        length = 0;
    }
    ok = false;
}

LineParser::LineParser() {
    reset();
}

void LineParser::addChar(char in) {
    if ( next > capacity-2 ) {
        overflow = true;
    }
    else {
        buffer[next] = in;
        next++;
    }
}

void LineParser::reset() {
    memset( buffer, 0, capacity );
    next = 0;
    overflow = false;
}

void LineParser::process(char* in, char separator,  LineParsingResult& pr) {

    pr.reset();

    reset();

    util::trim( in );
    util::deleteRepeatedChars(in, ' ');

    for (size_t i=0; i<strlen(in); i++) {
        addChar(in[i]);
    }

    {
        pr.result = 0;
        pr.length = 0;
        pr.ok = false;
    }

    if ( overflow ) {
        return;
    }

    {
        int count = 0;
        char* token = index(buffer, separator);
        while( token != NULL ) {
            token = index(token+1, separator);
            count++;
        }
        pr.length = count+1;
        pr.result = new char*[pr.length];
    }

    {
        int count = 0;
        char sepString[] = {separator, '\0'};
        char* token = strtok(buffer, sepString);
        while( token != NULL && count < pr.length ) {
            util::trim( token );
            pr.result[count] = new char[strlen(token)+1];
            strcpy( pr.result[count], token );
            token = strtok(NULL, sepString);
            count++;
        }
        pr.ok = (count == pr.length);
    }
}


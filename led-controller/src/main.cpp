#include <Arduino.h>
#include "usb_serial.h"
#include "line_parser.hpp"
#include "line_delimiter.hpp"
#include "util.hpp"
#include <cstdlib>
#include <TeensyID.h>

// PWM output pins
const uint8_t PIN_0 = 0;
const uint8_t PIN_1 = 1;
const uint8_t PIN_2 = 2;
const uint8_t PIN_3 = 3;
const uint8_t PIN_4 = 4;
const uint8_t PIN_5 = 5;
const uint8_t PIN_6 = 6;
const uint8_t PIN_7 = 7;
const uint8_t PIN_8 = 8;

// pins to learn on the synchronous control approach
const uint8_t PIN_9 = 9;
const uint8_t PIN_10 = 10;
const uint8_t PIN_11 = 11;

// analog input pins
const uint8_t PIN_14 = 14;
const uint8_t PIN_15 = 15;
const uint8_t PIN_16 = 16;
const uint8_t PIN_17 = 17;

// external interrupt pin 
const uint8_t PIN_21 = 21;

usb_serial_class serial;
IntervalTimer timer;
LineDelimiter lineDelimiter;
LineParser lineParser;
LineDelimitationResult lineResult;
LineParsingResult parsingResult;

void printToConsole( const char * format, ... ) {
    static uint64_t line_count = 0;
    static char buffer[256];

    va_list args;
    va_start( args, format );
    memset(buffer, 0, sizeof(buffer));
    vsnprintf( buffer, sizeof(buffer)-1, format, args );
    va_end( args );

    serial.printf( "[%s]", teensySN() );
    serial.printf( " %06d ", line_count++ ); 
    serial.printf( "%s", buffer );
    serial.printf( "\r\n" );
}

void setPwm ( int pwmArray[] ) {
    analogWrite(PIN_0, pwmArray[0]);
    analogWrite(PIN_1, pwmArray[1]);
    analogWrite(PIN_2, pwmArray[2]);
    analogWrite(PIN_3, pwmArray[3]);
    analogWrite(PIN_4, pwmArray[4]);
    analogWrite(PIN_5, pwmArray[5]);
    analogWrite(PIN_6, pwmArray[6]);
    analogWrite(PIN_7, pwmArray[7]);
    analogWrite(PIN_8, pwmArray[8]);
}

void setAllPwmOnOffAtOnce ( bool in ) {
    const int level = in ? 1024 : 0;
    analogWrite(PIN_0, level);
    analogWrite(PIN_1, level);
    analogWrite(PIN_2, level);
    analogWrite(PIN_3, level);
    analogWrite(PIN_4, level);
    analogWrite(PIN_5, level);
    analogWrite(PIN_6, level);
    analogWrite(PIN_7, level);
    analogWrite(PIN_8, level);
}

bool processLedLine( LineParsingResult& parsingResult ) {
    if ( parsingResult.length != 2 ) {
        return false;
    }
    if ( strcmp( parsingResult.result[1], "on" ) == 0 ) {
        digitalWrite(LED_BUILTIN, HIGH);
        return true;
    }
    if ( strcmp( parsingResult.result[1], "off" ) == 0 ) {
        digitalWrite(LED_BUILTIN, LOW);
        return true;
    }
    return false;
}

int scalePercentage( int percentage, int range ) {
    
    if (percentage < 0) {
        percentage = 0;
    }
    
    if (percentage > 100) {
        percentage = 100;
    }

    return ( ( (float) percentage ) * 0.01 * range );
}

bool processPwmLine( LineParsingResult& parsingResult ) {

    if ( parsingResult.length == 2 ) {
        if ( strcmp( parsingResult.result[1], "on" ) == 0 ) {
            setAllPwmOnOffAtOnce(true);
            return true;
        }
        if ( strcmp( parsingResult.result[1], "off" ) == 0 ) {
            setAllPwmOnOffAtOnce(false);
            return true;
        }
    }

    const int N_PWM = 9;
    if ( parsingResult.length != (N_PWM+1) ) {
        return false;
    }

    int pwmArray[N_PWM] = {0};
    util::AtoiResult pwmValue;
    for ( int i=1; i<parsingResult.length; i++ ) {   // ignore first token, it is the command
        util::atoi(parsingResult.result[i], pwmValue);
        if ( !pwmValue.valid ) {
            return false;
        }
        pwmArray[i-1] = scalePercentage( pwmValue.value, 1024 );
    }

    setPwm(pwmArray);
    return true;
}

void printHelp() {
    printToConsole("");
    printToConsole("led-controller app - version 0.0.1");
    printToConsole("command line options:");
    printToConsole("  help                  show this info");
    printToConsole("  led on|off            tests the board by showing led activity");
    printToConsole("  pwm on|off            shuts down or turns up all 10");
    printToConsole("  pwm n1 n2 n3 .. n10   sets all 10 duty cycles 0-100");
    printToConsole("");
}

bool processInputLine( LineParsingResult& parsingResult ) {
    
    if ( !parsingResult.ok || parsingResult.length < 1) {
        return false;
    }

    if ( strcmp( parsingResult.result[0], "help" ) == 0 ) {
        printHelp();
        return true;
    }

    if ( strcmp( parsingResult.result[0], "led" ) == 0 ) {
        return processLedLine( parsingResult );
    }

    if ( strcmp( parsingResult.result[0], "pwm" ) == 0 ) {
        return processPwmLine( parsingResult );
    }

    return false;
}

void handleSerialLine(uint64_t tickCount) {

    if ( tickCount % 10 > 0 ) {
        return;
    }

    while ( serial.available() ) {
        lineDelimiter.addChar((char)serial.read(), lineResult);
        if ( lineResult.ok ) {
            if ( strlen(lineResult.result) == 0 ) {
                printHelp();
                continue;
            }

            lineParser.process( lineResult.result, ' ', parsingResult );
            bool status = processInputLine( parsingResult );
            if (!status) {
                printToConsole("unknown command: [%s]", lineResult.result);
                printToConsole("in case you need some help, please find it below");
                printHelp();
            }
            printToConsole("ready");
        }
    }
}

void interruptCallback() {
    setAllPwmOnOffAtOnce(false);
}

void handleFastTickPin(uint64_t tickCount) {
    digitalWrite(PIN_11, (tickCount % 2 == 0) ? HIGH: LOW);
}

void handleComplexOutput(uint64_t tickCount) {
    const uint64_t modulus =  tickCount % 100;
    const bool turnOnCondition = ( modulus > 43 && modulus < 64 );
    digitalWrite(PIN_9, turnOnCondition ? HIGH: LOW);   
    digitalWrite(PIN_10, turnOnCondition ? HIGH: LOW);   
}

void handleAnalogRead(uint64_t tickCount) {

    // internal state for this function only
    static int last_analog_read_reported = -1;

    if ( tickCount % 100 > 0 ) {
        return;
    }

    const int analog_read = analogRead(PIN_17);

    // use some hysteresis to mitigate too much terminal scrolling due to noise on the high Z analog lines
    if ( abs(last_analog_read_reported - analog_read) > 30 ) {
        printToConsole("analog read:%d", analog_read);
        last_analog_read_reported = analog_read;
    }
}

void timerCallback() {
    static uint64_t tickCount = 0;

    handleFastTickPin(tickCount);
    handleComplexOutput(tickCount);
    handleAnalogRead(tickCount);
    handleSerialLine(tickCount);

    tickCount++;
}

void setup() {

    // PWM pins
    pinMode(PIN_0, OUTPUT);
    pinMode(PIN_1, OUTPUT);
    pinMode(PIN_2, OUTPUT);
    pinMode(PIN_3, OUTPUT);
    pinMode(PIN_4, OUTPUT);
    pinMode(PIN_5, OUTPUT);
    pinMode(PIN_6, OUTPUT);
    pinMode(PIN_7, OUTPUT);
    pinMode(PIN_8, OUTPUT);

    // sync control testing pins
    pinMode(PIN_9, OUTPUT);
    pinMode(PIN_10, OUTPUT);
    pinMode(PIN_11, OUTPUT);

    // internal LED pin
    pinMode(LED_BUILTIN, OUTPUT);

    // analog input
    pinMode(PIN_14, INPUT);
    pinMode(PIN_15, INPUT);
    pinMode(PIN_16, INPUT);
    pinMode(PIN_17, INPUT);
    
    // external interrupt
    pinMode(PIN_21, INPUT);

    attachInterrupt( PIN_21, interruptCallback, HIGH );
    timer.begin(timerCallback, 100);

    analogWriteResolution(10);
    digitalWrite(LED_BUILTIN, HIGH);
}

void loop() {}

